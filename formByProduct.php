<?php
/*
Plugin Name: Form By Product 
Plugin URI: https://gitlab.com/luism23/formbyproduct
Description:
Author: LuisMiguelNarvaez
Version: 1.0.6
Author URI: https://gitlab.com/luism23
License: GPL
 */
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/formbyproduct',
	__FILE__,
	'formbyproduct'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');


define("FORMBYPRODUCT_LOG",false);
define("FORMBYPRODUCT_PATH",plugin_dir_path(__FILE__));
define("FORMBYPRODUCT_URL",plugin_dir_url(__FILE__));

require FORMBYPRODUCT_PATH."src/_index.php";