<?php
if(FORMBYPRODUCT_LOG){
    function add_FORMBYPRODUCT_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'FORMBYPRODUCT_LOG',
                'title' => 'FORMBYPRODUCT_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=FORMBYPRODUCT_LOG'
            )
        );
    }

    function FORMBYPRODUCT_LOG_option_page()
    {
        add_options_page(
            'Log Alegra',
            'Log Alegra',
            'manage_options',
            'FORMBYPRODUCT_LOG',
            'FORMBYPRODUCT_LOG_page'
        );
    }

    function FORMBYPRODUCT_LOG_page()
    {
        $log = get_option("FORMBYPRODUCT_LOGS");
        if($log === false || $log == null || $log == ""){
            $log = "[]";
        }
        ?>
        <script>
            const log = <?=$log?>;
        </script>
        <h1>
            Solo se guardan las 100 peticiones
        </h1>
        <pre><?php var_dump(array_reverse(json_decode($log,true)));?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_FORMBYPRODUCT_LOG_option_page', 100);

    add_action('admin_menu', 'FORMBYPRODUCT_LOG_option_page');

}

function addFORMBYPRODUCT_LOG($newLog)
{
    if(!FORMBYPRODUCT_LOG){
        return;
    }
    $log = get_option("FORMBYPRODUCT_LOGS");
    if($log === false || $log == null || $log == ""){
        $log = "[]";
    }
    $log = json_decode($log);
    $log[] = $newLog;
    $log = array_slice($log, -100,100); 
    update_option("FORMBYPRODUCT_LOGS",json_encode($log));
}