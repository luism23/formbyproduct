<?php

function FORMBYPRODUCT_printForms()
{
    $user_id = get_current_user_id();

    $customer_orders = get_posts(array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => $user_id,
        'post_type' => wc_get_order_types(),
        'post_status' => array('wc-processing'),
    ));
    $products_ids = [];
    foreach ($customer_orders as $customer_order) {
        $order = wc_get_order($customer_order);
        foreach ( $order->get_items() as $item_id => $item ) {
            $product_id = $item->get_product_id();
            $variation_id = $item->get_variation_id();
            
            if($variation_id!=0){
                $product_id = $variation_id;
            }

            $products_ids[$product_id] = $product_id;
        }
    }
    foreach ($products_ids as $product_id => $value) {
        $product = wc_get_product( $product_id );
        $template_id = get_post_meta($product_id,"template",true);
        ?>
        <h1>
            <?=$product->get_name()?>
        </h1>
        <?php
        echo do_shortcode('[elementor-template id="'.$template_id.'"]');
    }
}
add_shortcode( 'FORMBYPRODUCT_printForms', 'FORMBYPRODUCT_printForms' );